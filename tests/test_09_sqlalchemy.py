import os.path
from typing import List

import pytest
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base

from sync.backends.sqlalchemy import SQADataSource
from demo.daphne import VlanServiceConnection
from demo.desire import NIC
from sync.mapping import Mapping, Mode, AttributeMap
from sync.mixins import LinkProvider
from sync.operations import (
    Addition,
    Linking,
    SkipUpdateOrphanedEntity,
    DeletedOrphanedEntity,
)
from sync.syncer import Syncer

A_PATH = "tests/resources/a.sqlite"
B_PATH = "tests/resources/b.sqlite"

ABase = declarative_base()
BBase = declarative_base()


class A(ABase):
    __tablename__ = "a"
    id = Column(Integer, primary_key=True)
    name = Column(String)


class B(BBase):
    __tablename__ = "b"
    id = Column(Integer, primary_key=True)
    name = Column(String)


@pytest.fixture
def sqlalchemy_datasource_a():
    sa_data_source = SQADataSource(f"sqlite:///{A_PATH}", ABase)
    yield sa_data_source
    os.remove(A_PATH)


@pytest.fixture
def sqlalchemy_datasource_b():
    sa_data_source = SQADataSource(f"sqlite:///{B_PATH}", BBase)
    yield sa_data_source
    os.remove(B_PATH)


@pytest.fixture
def a_b_mapping():
    yield [
        Mapping(
            modes={Mode.LEFT_TO_RIGHT},
            entity_types=(A, B),
            keys={AttributeMap(from_attribute="name", to_attribute="name")},
        )
    ]


def test_create_id_find(sqlalchemy_datasource_a: SQADataSource):
    a = A(name="a")
    sqlalchemy_datasource_a.persist(a)
    assert a.id is not None
    a_id = sqlalchemy_datasource_a.id(a)
    assert a_id == a.id
    assert sqlalchemy_datasource_a.find(A, {"name": "a"})[0] == a
    assert not sqlalchemy_datasource_a.get(A, 50)


def test_sync(
    sqlalchemy_datasource_a: SQADataSource,
    sqlalchemy_datasource_b: SQADataSource,
    a_b_mapping: List[Mapping],
    redis_link_provider: LinkProvider,
):
    a = A(name="a")
    sqlalchemy_datasource_a.create(a)

    syncer = Syncer(
        sqlalchemy_datasource_a,
        sqlalchemy_datasource_b,
        a_b_mapping,
        redis_link_provider,
    )

    operations = syncer.sync_all()
    b = sqlalchemy_datasource_b.all(B)[0]
    assert operations[0] == Addition(
        from_class=A,
        to_class=B,
        from_entity=a,
        to_entity=b,
    )
    assert operations[1] == Linking(
        classes=frozenset({A, B}), entities=frozenset({a, b})
    )


def test_sqa_on_one_side(
    daphne_db,
    desire_db,
    daphne_desire_all_mappings: List[Mapping],
    redis_link_provider,
):
    syncer = Syncer(
        daphne_db,
        desire_db,
        daphne_desire_all_mappings,
        link_provider=redis_link_provider,
    )

    for index in range(3):
        operations = syncer.sync_all()

    assert operations == []

    length_nics = len(desire_db.all(NIC))
    length_vlscs = len(daphne_db.all(VlanServiceConnection))

    assert length_nics, length_vlscs

    nic2 = desire_db.get(NIC, 2)
    nic2_daphne = redis_link_provider.others(
        entity=nic2,
        entity_ds=desire_db,
        other_type=VlanServiceConnection,
        other_ds=daphne_db,
    )[0]

    desire_db.delete(nic2)
    daphne_desire_all_mappings[-1].delete_orphaned_entities = True

    operations = syncer.sync_all()

    assert len(operations) == 2

    assert operations[0] == SkipUpdateOrphanedEntity(
        from_class=VlanServiceConnection,
        to_class=NIC,
        from_entity=nic2_daphne,
        to_entity=None,
    )
    assert operations[1] == DeletedOrphanedEntity(nic2_daphne)

    assert desire_db.get(NIC, 2) is None
    assert len(daphne_db.find(VlanServiceConnection, {"name": nic2_daphne.name})) == 0

    length_nics_after_deletion = len(desire_db.all(NIC))
    length_vlscs_after_deletion = len(daphne_db.all(VlanServiceConnection))

    # Only one object disappeared on both sides
    assert length_nics_after_deletion == length_nics - 1
    assert length_vlscs_after_deletion == length_vlscs - 1

    operations = syncer.sync_all()

    # Nothing else happens afterwards
    assert len(operations) == 0
