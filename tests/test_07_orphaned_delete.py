from typing import List

from sync.backends.inmemory import InMemoryLinkProvider
from demo.daphne import VlanServiceProvider, VlanServiceConnection
from demo.desire import CloudServiceProvider, NIC
from sync.datasource import DataSource
from sync.mapping import Mapping
from sync.operations import SkipUpdateOrphanedEntity, DeletedOrphanedEntity
from sync.syncer import Syncer


def test_collect_other_classes(daphne_db, desire_db, daphne_desire_all_mappings):
    syncer = Syncer(daphne_db, desire_db, daphne_desire_all_mappings)
    generating_classes = syncer._collect_linked_classes(NIC)

    assert generating_classes == {VlanServiceConnection}


def test_is_orphaned(daphne_db, desire_db, daphne_desire_all_mappings):
    syncer = Syncer(daphne_db, desire_db, daphne_desire_all_mappings)
    syncer.sync_all()

    vlsc = daphne_db.get(VlanServiceConnection, 1)
    linked_nic = next(
        iter(syncer.link_provider.others(vlsc, daphne_db, NIC, desire_db))
    )
    daphne_db.delete(vlsc)

    assert syncer._is_orphaned(
        {VlanServiceConnection},
        linked_nic,
        desire_db,
        daphne_db,
    )

    desire_db.delete(linked_nic)

    for nic in desire_db.all(NIC):
        assert not syncer._is_orphaned(
            {VlanServiceConnection}, nic, desire_db, daphne_db
        )


def test_collect_orphans(daphne_db, desire_db, daphne_desire_all_mappings):
    syncer = Syncer(daphne_db, desire_db, daphne_desire_all_mappings)
    syncer.sync_all()

    vlsc = daphne_db.get(VlanServiceConnection, 1)
    linked_nic = next(
        iter(syncer.link_provider.others(vlsc, daphne_db, NIC, desire_db))
    )
    daphne_db.delete(vlsc)

    assert [] == syncer._collect_orphaned_entities(
        daphne_db, VlanServiceConnection, desire_db
    )
    assert [linked_nic] == syncer._collect_orphaned_entities(desire_db, NIC, daphne_db)


def test_delete_orphaned_entities(
    desire_db: DataSource,
    daphne_db: DataSource,
    delete_orphaned_entities_mapping: List[Mapping],
):
    link_provider = InMemoryLinkProvider()
    syncer = Syncer(
        daphne_db, desire_db, delete_orphaned_entities_mapping, link_provider
    )
    syncer.sync_all()

    csp_ibm = desire_db.find(CloudServiceProvider, {"name": "IBM"})[0]
    vlsp_ibm = next(
        iter(link_provider.others(csp_ibm, desire_db, VlanServiceProvider, daphne_db))
    )

    # removing on right side
    desire_db.delete(csp_ibm)
    assert len(desire_db.find(CloudServiceProvider, {"name": "IBM"})) == 0

    operations = syncer.sync_all()

    assert len(operations) == 2

    assert operations[0] == SkipUpdateOrphanedEntity(
        VlanServiceProvider, CloudServiceProvider, vlsp_ibm, csp_ibm
    )

    assert operations[1] == DeletedOrphanedEntity(vlsp_ibm)

    assert len(desire_db.find(CloudServiceProvider, {"name": "IBM"})) == 0
    assert len(daphne_db.find(VlanServiceProvider, {"name": "IBM"})) == 0

    operations = syncer.sync_all()
    assert len(operations) == 0
