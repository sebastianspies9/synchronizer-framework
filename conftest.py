import os

import sys

sys.path.append("src/")

from sync.backends.redislinkprovider import RedisLinkProvider
from sync.backends.sqlalchemy import SQADataSource
from sync.mixins import LinkProvider
from typing import List
import pytest
from sync.mapping import DirtyMode, Mapping, Mode, AttributeMap
from demo.daphne import VlanServiceProvider, VlanServiceConnection
from demo.desire import (
    CloudServiceProvider,
    CloudRegion,
    CloudHandover,
    NIC,
    DesireBase,
)
from sync.backends.inmemory import InMemoryDs, InMemoryBase


@pytest.fixture
def desire_db():
    return InMemoryDs.from_collection(
        {
            CloudServiceProvider(
                name="AWS",
                children=[
                    CloudRegion(
                        name="eu-central-1",
                        children=[
                            CloudHandover(name="INX6", children=[NIC(name="nic-1")]),
                            CloudHandover(name="EqFA5", children=[NIC(name="nic-2a")]),
                        ],
                    ),
                    CloudRegion(
                        name="eu-west-1",
                        children=[
                            CloudHandover(name="LON1", children=[NIC(name="nic-3a")])
                        ],
                    ),
                    CloudRegion(
                        name="eu-west-2",
                        children=[
                            CloudHandover(
                                name="EqFA5",
                                children=[NIC(name="nic-3", external_ref="nic3-eref")],
                            )
                        ],
                    ),
                ],
            ),
            CloudServiceProvider(
                name="IBM",
                children=[
                    CloudRegion(
                        name="EU-Frankfurt",
                        children=[
                            CloudHandover(name="fra03", children=[NIC(name="nic-4")])
                        ],
                    )
                ],
            ),
        }
    )


@pytest.fixture
def daphne_db():
    return InMemoryDs.from_collection(
        {
            VlanServiceProvider(
                name="AWS",
                children=[
                    VlanServiceConnection(
                        name="nic-1", region="eu-central-1", pop="INX6"
                    ),
                    VlanServiceConnection(
                        name="nic-2", region="eu-central-1", pop="EqFA5"
                    ),
                    VlanServiceConnection(
                        name="nic-3", region="eu-west-2", pop="EqFA5"
                    ),
                    VlanServiceConnection(
                        name="nic-5", region="ap-southeast-1", pop="KINX"
                    ),
                ],
            ),
            VlanServiceProvider(name="AZURE", children=[]),
        }
    )


@pytest.fixture
def simple_mapping():
    return [
        Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.LEFT_TO_RIGHT, Mode.RIGHT_TO_LEFT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
        )
    ]

@pytest.fixture
def conditional_mapping():
    return [
        Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.LEFT_TO_RIGHT, Mode.RIGHT_TO_LEFT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
            left_conditions={"name": "AWS"},
            right_conditions={"name": "AZURE"}
        )
    ]

@pytest.fixture
def delete_orphaned_entities_mapping():
    return [
        Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.LEFT_TO_RIGHT, Mode.RIGHT_TO_LEFT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
            delete_orphaned_entities=True,
        )
    ]


@pytest.fixture
def daphne_desire_mappings() -> List[Mapping]:
    return [
        Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.LEFT_TO_RIGHT, Mode.RIGHT_TO_LEFT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
        ),
        Mapping(
            entity_types=(VlanServiceConnection, CloudRegion),
            modes={Mode.LEFT_TO_RIGHT},
            keys={
                AttributeMap("region", "name"),
                AttributeMap(
                    "parent", "parent", VlanServiceProvider, CloudServiceProvider
                ),
            },
        ),
    ]


@pytest.fixture
def daphne_desire_dirty_ignore() -> Mapping:
    return Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.LEFT_TO_RIGHT, Mode.RIGHT_TO_LEFT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
            dirty_mode=DirtyMode.IGNORE
        )

@pytest.fixture
def daphne_desire_dirty_do_not_overwrite() -> Mapping:
    return Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.RIGHT_TO_LEFT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
            dirty_mode=DirtyMode.DO_NOT_OVERWRITE
        )

@pytest.fixture
def daphne_desire_dirty_update() -> Mapping:
    return Mapping(
            entity_types=(VlanServiceProvider, CloudServiceProvider),
            modes={Mode.LEFT_TO_RIGHT},
            keys={AttributeMap("name", "name")},
            attributes={AttributeMap("canUpgrade", "upgrade_allowed")},
            dirty_mode=DirtyMode.UPDATE
        )


@pytest.fixture
def daphne_desire_link_sync_mappings() -> List[Mapping]:
    return [
        Mapping(
            entity_types=(VlanServiceConnection, CloudHandover),
            modes={Mode.LEFT_TO_RIGHT},
            keys={
                AttributeMap("pop", "name"),
                AttributeMap("__self__", "parent", VlanServiceConnection, CloudRegion),
            },
        ),
        Mapping(
            entity_types=(VlanServiceConnection, NIC),
            modes={Mode.LEFT_TO_RIGHT},
            keys={
                AttributeMap("name", "name"),
                AttributeMap(
                    "__self__", "parent", VlanServiceConnection, CloudHandover
                ),
            },
            attributes={AttributeMap("externalRef", "external_ref")},
        ),
    ]


@pytest.fixture()
def daphne_desire_mappings_deep_joins() -> List[Mapping]:
    return [
        Mapping(
            entity_types=(VlanServiceConnection, NIC),
            modes={Mode.RIGHT_TO_LEFT},
            keys={
                AttributeMap("name", "name"),
                AttributeMap("region", ("parent", "parent", "name")),
                AttributeMap("pop", ("parent", "name")),
                AttributeMap(
                    "parent",
                    ("parent", "parent", "parent"),
                    VlanServiceProvider,
                    CloudServiceProvider,
                ),
            },
            attributes={AttributeMap("externalRef", "external_ref")},
        ),
    ]


@pytest.fixture
def daphne_desire_all_mappings(
    daphne_desire_mappings,
    daphne_desire_link_sync_mappings,
    daphne_desire_mappings_deep_joins,
):
    return [
        *daphne_desire_mappings,
        *daphne_desire_link_sync_mappings,
        *daphne_desire_mappings_deep_joins,
    ]


@pytest.fixture()
def desire_db_on_sqa(desire_db: InMemoryDs):
    sqlite = "/tmp/desire_test.sqlite"
    desire_sqa_ds = SQADataSource(url=("sqlite:///%s" % sqlite), base=DesireBase)
    for values in desire_db.store.values():
        for value in values:
            desire_sqa_ds.persist(value)
    yield desire_sqa_ds
    os.remove(sqlite)


@pytest.fixture
def a_b_mappings():
    return [
        Mapping(
            modes={Mode.LEFT_TO_RIGHT, Mode.RIGHT_TO_LEFT},
            entity_types=(A, B),
            keys={AttributeMap(from_attribute="key_a", to_attribute="key_b")},
            attributes={AttributeMap(from_attribute="prop_a", to_attribute="prop_b")},
        )
    ]


class A(InMemoryBase):
    def __init__(self, key_a=None, prop_a=None):
        super().__init__()
        self.key_a = key_a
        self.prop_a = prop_a

    key_a: str
    prop_a: str


class B(InMemoryBase):
    def __init__(self, key_b=None, prop_b=None):
        super().__init__()
        self.key_b = key_b
        self.prop_b = prop_b

    key_b: str
    prop_b: str


class Parent:
    def __init__(self):
        super().__init__()

    key_parent: str
    attribute_parent: str


class Child:
    def __init__(self):
        super().__init__()

    parent: Parent
    key_child: str
    attribute_child: str


class Child2:
    def __init__(self):
        super().__init__()

    key_child2: str
    key2_child2: str
    attribute_one: str
    attribute_two: str


def deep_join_mappings() -> List[Mapping]:
    return [
        Mapping(
            modes=[Mode.LEFT_TO_RIGHT],
            entity_types=(Child, Child2),
            keys={
                AttributeMap("key_child", "key_child2"),
                AttributeMap(("parent", "key_parent"), "key2_child2"),
            },
            attributes={
                AttributeMap("attribute_child", "attribute_one"),
                AttributeMap(("parent", "attribute_parent"), "attribute_two"),
            },
        )
    ]


@pytest.fixture()
def redis_link_provider() -> LinkProvider:
    redis_host = os.getenv("REDIS_TEST_HOST", "redis")
    redis_link_provider = RedisLinkProvider(host=redis_host)
    yield redis_link_provider
    redis_link_provider.redis.flushdb()
