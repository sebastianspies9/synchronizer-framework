from demo.desire import CloudRegion, CloudHandover
from sync.datasource import DataSource
from sync.syncer import Syncer


def test_dependent(
    daphne_db: DataSource,
    desire_db: DataSource,
    daphne_desire_mappings,
    daphne_desire_link_sync_mappings,
):
    mappings = [*daphne_desire_mappings, *daphne_desire_link_sync_mappings]

    syncer = Syncer(daphne_db, desire_db, mappings)
    syncer.sync_all()

    result = desire_db.find(CloudRegion, {"name": "ap-southeast-1"})
    assert len(result) == 1
    cloud_region = result[0]

    pop = desire_db.find(CloudHandover, {"name": "KINX", "parent": cloud_region})
    assert len(pop) == 1
