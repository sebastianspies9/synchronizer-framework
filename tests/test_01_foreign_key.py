from sync.backends.inmemory import InMemoryDs

from demo.daphne import (
    VlanServiceProvider,
    VlanServiceConnection,
)
from demo.desire import (
    CloudServiceProvider,
    CloudRegion,
)
from sync.operations import SkipMissingLink, Linking
from sync.syncer import Syncer


def test_unmatched(daphne_desire_mappings):
    daphne_desire_mappings_cr_vlsc_only = [daphne_desire_mappings[1]]

    from_entity = VlanServiceConnection(name="VLSC1", region="Region1", pop="Pop1")
    db_daphne = InMemoryDs.from_collection(
        [
            VlanServiceProvider(
                name="CSP1",
                children=[from_entity],
            )
        ]
    )

    db_desire = InMemoryDs.from_collection(
        [
            CloudRegion(name="Region1"),
        ]
    )

    syncer = Syncer(db_daphne, db_desire, daphne_desire_mappings_cr_vlsc_only)

    operations = syncer.sync_all()
    assert len(operations) == 1
    operations[0] == SkipMissingLink(
        from_class=VlanServiceConnection, to_class=CloudRegion, from_entity=from_entity
    )


def test_matched(daphne_desire_mappings):
    from_entity = VlanServiceConnection(name="VLSC1", region="Region1", pop="Pop1")
    reversed_mappings = daphne_desire_mappings.copy()
    reversed_mappings.reverse()

    db_daphne = InMemoryDs.from_collection(
        {
            VlanServiceProvider(
                name="CSP1",
                children=[from_entity],
            )
        }
    )

    db_desire = InMemoryDs.from_collection(
        {
            CloudServiceProvider(
                name="CSP1",
                children=[
                    CloudRegion(name="Region1"),
                ],
            )
        }
    )

    # First try
    syncer = Syncer(db_daphne, db_desire, reversed_mappings)
    operations = syncer.sync_all()

    assert len(operations) == 2

    # Not synchronizing, because parent of VlanServiceConnection cannot be found (CSP1 objects not linked yet)
    assert operations[0] == SkipMissingLink(
        from_class=VlanServiceConnection,
        from_entity=from_entity,
        to_class=CloudRegion,
    )

    # Linking CSP1 objects
    assert operations[1] == Linking(
        classes=frozenset({CloudServiceProvider, VlanServiceProvider}),
        entities=operations[1].entities,
    )

    # Second try, now linking regions
    operations = syncer.sync_all()
    assert len(operations) == 1
    assert operations[0] == Linking(
        classes=frozenset({VlanServiceConnection, CloudRegion}),
        entities=operations[0].entities,
    )
